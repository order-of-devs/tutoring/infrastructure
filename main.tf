# Narzedzie do zarzadzania gitlab
# Tworzenie projektu
# Przypysywanie automatycznie osob
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_users" "example" {
  for_each = toset(["mkubasz", "Czakuou"])

  search = each.value
}

resource "gitlab_group" "example_group" {
  name = var.group_name
  path = var.group_name
  parent_id = 12411650
}

resource "gitlab_group_membership" "test_group_member" {
  depends_on = [gitlab_group.example_group]
  access_level = "developer"
  group_id     = gitlab_group.example_group.id

  for_each = data.gitlab_users.example
  user_id      = each.value.users[0].id
}

resource "gitlab_project" "test_project" {
  depends_on = [gitlab_group.example_group]
  name = var.project_name
  visibility_level = "private"
  namespace_id = gitlab_group.example_group.id
}
