variable "group_name" {
  type = string
  default = "example_group"
}

variable "project_name" {
  type = string
  default = "example_project"
}

variable "gitlab_token" {
  type = string
  description = "gitlab access token"
  sensitive = true
}